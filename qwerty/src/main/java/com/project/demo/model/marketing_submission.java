package com.project.demo.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="marketing_submission")
public class marketing_submission {
	
	private int id;
	private int consultant_id;
	private int employee_id;
	private String title;
	private String status;
	private Timestamp created;
	private String job_location;
	
	public marketing_submission() {
		
	}

	public marketing_submission(int id, int consultant_id, int employee_id, String title, String status,
			Timestamp created,String job_location) {
		
		this.id = id;
		this.consultant_id = consultant_id;
		this.employee_id = employee_id;
		this.title = title;
		this.status = status;
		this.created = created;
		this.job_location = job_location;
		
	}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	@Column(name="consultant_id", nullable=true)
    public int getConsultant_id() {
		return consultant_id;
	}

	public void setConsultant_id(int consultant_id) {
		this.consultant_id = consultant_id;
	}
	@Column(name="employee_id", nullable=false)
	public int getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(int employee_id) {
		this.employee_id = employee_id;
	}

	@Column(name = "title", nullable = false)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@Column(name = "status", nullable = true)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	@Column(name = "created", nullable = false)
	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}
	@Column(name = "job_location", nullable = false)
	public String getJob_location() {
		return job_location;
	}

	public void setJob_location(String job_location) {
		this.job_location = job_location;
	}
	
	
	

}
