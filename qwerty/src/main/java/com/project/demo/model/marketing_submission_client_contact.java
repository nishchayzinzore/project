package com.project.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="marketing_submission_client_contact")
public class marketing_submission_client_contact {
	
	private int id;
	private int clientcontact_id;
	private int submission_id;
	
	public marketing_submission_client_contact() {
		
	}

	public marketing_submission_client_contact(int id, int clientcontact_id, int submission_id) {
		
		this.id = id;
		this.clientcontact_id = clientcontact_id;
		this.submission_id = submission_id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	@Column(name = "clientcontact_id", nullable = false)
	public int getClientcontact_id() {
		return clientcontact_id;
	}

	public void setClientcontact_id(int clientcontact_id) {
		this.clientcontact_id = clientcontact_id;
	}
	@Column(name = "submission_id", nullable = false)
	public int getSubmission_id() {
		return submission_id;
	}

	public void setSubmission_id(int submission_id) {
		this.submission_id = submission_id;
	}
	
	
	
	

}
