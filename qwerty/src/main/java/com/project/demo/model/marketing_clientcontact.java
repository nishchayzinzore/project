package com.project.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="marketing_clientcontact")
public class marketing_clientcontact {
	
	private int id;
	private int client_id;
	
	public marketing_clientcontact() {
		
	}
	public marketing_clientcontact(int id, int client_id) {
		
		this.id = id;
		this.client_id = client_id;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Column(name = "client_id", nullable = false)
	public int getClient_id() {
		return client_id;
	}
	public void setClient_id(int client_id) {
		this.client_id = client_id;
	}

}
