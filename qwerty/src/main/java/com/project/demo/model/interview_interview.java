package com.project.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="interview_interview")

public class interview_interview {
	
	private int id;
	private String title;
	private int interview_supervisor_id;
	private int submission_id;

	
	public interview_interview() {
		
	}

	
	

	public interview_interview(int id, String title, int interview_supervisor_id, int submission_id) {
		
		this.id = id;
		this.title = title;
		this.interview_supervisor_id = interview_supervisor_id;
		this.submission_id = submission_id;
	}




	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	
	
	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "title", nullable = false)
	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "interview_supervisor_id", nullable = false)
	public int getInterview_supervisor_id() {
		return interview_supervisor_id;
	}


	public void setInterview_supervisor_id(int interview_supervisor_id) {
		this.interview_supervisor_id = interview_supervisor_id;
	}

	@Column(name = "submission_id", nullable = false)
	public int getSubmission_id() {
		return submission_id;
	}


	public void setSubmission_id(int submission_id) {
		this.submission_id = submission_id;
	}
	
	

}
