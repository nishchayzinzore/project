package com.project.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="consultants_consultant")
public class consultants_consultant {
	
	private int id;
	private String ssn;
	private int user_id;
    private int recruiter_id;
    private String curr_loc;
    private String title;
    
    
	
	public consultants_consultant() {
		
	}

	public consultants_consultant(int id,String ssn,int user_id,int recruiter_id,String curr_loc,String title) {
	
		this.id = id;
		this.ssn = ssn;
		this.user_id=user_id;
		this.recruiter_id=recruiter_id;
		this.curr_loc=curr_loc;
		this.title=title;
		
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "ssn", nullable = false)
	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	@Column(name = "user_id", nullable = false)
	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	@Column(name = "recruiter_id", nullable = false)
	public int getRecruiter_id() {
		return recruiter_id;
	}

	public void setRecruiter_id(int recruiter_id) {
		this.recruiter_id = recruiter_id;
	}
    
	@Column(name = "curr_loc", nullable = false)
	public String getCurr_loc() {
		return curr_loc;
	}

	public void setCurr_loc(String curr_loc) {
		this.curr_loc = curr_loc;
	}
	
	@Column(name = "title", nullable = false)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	  
	

}
