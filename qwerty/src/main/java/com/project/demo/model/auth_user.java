package com.project.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "auth_user")
public class auth_user {
	 private int id;
	    private String first_name;
	    private String last_name;
	    
	 
	    public auth_user() {
	  
	    }
	 
	    public auth_user(String first_name, String last_name, int id) {
	         this.first_name = first_name;
	         this.last_name = last_name;
	         this.id = id;
	    }
	 
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	        public int getId() {
	        return id;
	    }
	    public void setId(int id) {
	        this.id = id;
	    }
	 
	    @Column(name = "first_name", nullable = false)
	    public String getFirst_name() {
	        return first_name;
	    }
	    public void setFirst_name(String first_name) {
	        this.first_name = first_name;
	    }
	 
	    @Column(name = "last_name", nullable = false)
	    public String getLast_name() {
	        return last_name;
	    }
	    public void setLast_name(String last_name) {
	        this.last_name = last_name;
	    }
	 
	    
	    

	    @Override
	    public String toString() {
	        return "Employee [id=" + id + ", first_name=" + first_name + ", last_name=" + last_name 
	       + "]";
	    }
}
