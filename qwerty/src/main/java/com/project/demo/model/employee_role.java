package com.project.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="employee_role")
public class employee_role {

	private int id;
	private String title;
	
	public employee_role() {
		
	}
	public employee_role(int id, String title) {
		
		this.id = id;
		this.title = title;
	}
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    @Column(name = "title", nullable = false)
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
	
	
	
	
	
	
}
