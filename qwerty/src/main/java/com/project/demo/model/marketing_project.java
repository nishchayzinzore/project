package com.project.demo.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="marketing_project")
public class marketing_project {
	
	private int id;
	private int submission_id;
	private String title;
	private String project_type;
	private Timestamp start;

	
	public marketing_project() {
		
	}

	

	public marketing_project(int id, int submission_id, String title, String project_type, Timestamp start) {
		
		this.id = id;
		this.submission_id = submission_id;
		this.title = title;
		this.project_type = project_type;
		this.start = start;
	}



	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	@Column(name = "submission_id", nullable = false)
	public int getSubmission_id() {
		return submission_id;
	}



	public void setSubmission_id(int submission_id) {
		this.submission_id = submission_id;
	}


	@Column(name = "title", nullable = false)
	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}


	@Column(name = "project_type", nullable = true)
	public String getProject_type() {
		return project_type;
	}



	public void setProject_type(String project_type) {
		this.project_type = project_type;
	}


	@Column(name = "start", nullable = true)
	public Timestamp getStart() {
		return start;
	}



	public void setStart(Timestamp start) {
		this.start = start;
	}
	

	
}
