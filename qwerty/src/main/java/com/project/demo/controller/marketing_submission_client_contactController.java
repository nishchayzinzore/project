package com.project.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.demo.model.marketing_submission_client_contact;
import com.project.demo.repository.marketing_submission_client_contactRepository;
@CrossOrigin(origins="*",allowedHeaders = "*")
@RestController
@RequestMapping("/data")
public class marketing_submission_client_contactController {

	@Autowired
    private marketing_submission_client_contactRepository  marketing_submission_client_contactrepository;

    @GetMapping("/marketing_submission_client_contact")
    public List<marketing_submission_client_contact> getAllUsers() {
        return marketing_submission_client_contactrepository.findAll();
	
}
	
}
