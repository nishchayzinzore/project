package com.project.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.demo.repository.marketing_clientRepository;
import com.project.demo.model.marketing_client;
@CrossOrigin(origins="*",allowedHeaders = "*")
@RestController
@RequestMapping("/data")
public class marketing_clientController {

	@Autowired
    private marketing_clientRepository marketing_clientRepository;

    @GetMapping("/marketing_client")
    public List<marketing_client> getAllUsers() {
        return marketing_clientRepository.findAll();
}
}
