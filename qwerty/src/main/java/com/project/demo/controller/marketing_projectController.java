package com.project.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.demo.repository.IBeenMP;
import com.project.demo.repository.marketing_projectRepository;
@CrossOrigin(origins="*",allowedHeaders = "*")
@RestController
@RequestMapping("/data")
public class marketing_projectController {

	@Autowired
    private marketing_projectRepository marketing_projectrepository;

    @GetMapping("/marketing_project")
    public List<IBeenMP>getAllUsers() {
        return marketing_projectrepository.findByPO();
	
}
}
