package com.project.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.demo.model.employee_employee;
import com.project.demo.repository.employee_employeeRepository;

@CrossOrigin(origins="*",allowedHeaders = "*")
@RestController
@RequestMapping("/data")
public class employee_employeeController{

	
	@Autowired
    private employee_employeeRepository employee_employeerepository;

    @GetMapping("/employee_employee")
    public List<employee_employee> getAllEmp() {
        return employee_employeerepository.findAll();
    }
}
