package com.project.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.project.demo.repository.*;

@RestController
@RequestMapping("/data")
@CrossOrigin(origins="*",allowedHeaders = "*")
public class consultants_consultantController {
	@Autowired
   
 private consultants_consultantRepository consultants_consultantrepository;

    @GetMapping("/consultants_consultant")
    public List<IBeenCC> getAllUsers() {
        return consultants_consultantrepository.findByConsultants();
    }	
    
    @GetMapping("/consultants_consultant_sub/{cid}")
    public List<IBeenConsultantSubmission> getAllsu(@PathVariable("cid") int cid) {
        return consultants_consultantrepository.findByConsultantsub(cid);
    }	
    
    @GetMapping("/consultants_consultant_cli/{cid}")
    public List<IBeenConsultantClient> getAllcl(@PathVariable("cid") int cid) {
        return consultants_consultantrepository.findByConsultantcli(cid);
    }	
    @GetMapping("/consultants_consultant_int/{cid}")
    public List<IBeenConsultantInterview> getAllin(@PathVariable("cid") int cid) {
        return consultants_consultantrepository.findByConsultantint(cid);
    }	
    @GetMapping("/consultants_consultant_po/{cid}")
    public List<IBeenConsultantPo> getAllpo(@PathVariable("cid") int cid) {
        return consultants_consultantrepository.findByConsultantspo(cid);
    }	
	
}
