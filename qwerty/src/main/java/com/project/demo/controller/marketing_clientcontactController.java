package com.project.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.demo.model.marketing_clientcontact;
import com.project.demo.repository.marketing_clientcontactRepository;
@CrossOrigin(origins="*",allowedHeaders = "*")
@RestController
@RequestMapping("/data")
public class marketing_clientcontactController {
	
	@Autowired
    private marketing_clientcontactRepository marketing_clientcontactrepository;

    @GetMapping("/marketing_clientcontact")
    public List<marketing_clientcontact> getAllUsers() {
        return marketing_clientcontactrepository.findAll();
	
}
}
