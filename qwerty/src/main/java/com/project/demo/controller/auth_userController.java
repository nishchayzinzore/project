package com.project.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.demo.repository.IBeenMarketer;
import com.project.demo.repository.Marketer_List;


@CrossOrigin(origins="*",allowedHeaders = "*")
@RestController
@RequestMapping("/data")
public class auth_userController {
	
    @Autowired
    private Marketer_List marketer;
    
    
    @GetMapping("/marketersub")
    public List<IBeenMarketer> getMarketerSub() {
        return marketer.findByMarketerSub();
    }
    
    @GetMapping("/marketerint")
    public List<IBeenMarketer> getMarketerInt() {
        return marketer.findByMarketerInt();
    }
    
    @GetMapping("/marketerpo")
    public List<IBeenMarketer> getMarketerPo() {
        return marketer.findByMarketerPo();
    }

    
  
    
    

}
