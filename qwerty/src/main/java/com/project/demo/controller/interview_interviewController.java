package com.project.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.demo.repository.IBeenII;
import com.project.demo.repository.interview_interviewRepository;
@CrossOrigin(origins="*",allowedHeaders = "*")
@RestController
@RequestMapping("/data")
public class interview_interviewController {

	@Autowired
    private interview_interviewRepository interview_interviewrepository;

    @GetMapping("/interview_interview")
    public List<IBeenII> getAllUsers() {
        return interview_interviewrepository.findByInterviews();
	
}
}
