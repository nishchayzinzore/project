package com.project.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.demo.repository.IBeenCount;
import com.project.demo.repository.IBeenVC;
import com.project.demo.repository.marketing_submissionRepository;

@CrossOrigin(origins="*",allowedHeaders = "*")
@RestController
@RequestMapping("/data")
public class marketing_submissionController {

	@Autowired
    private marketing_submissionRepository marketing_submissionrepository;

	@GetMapping("/marketing_submission_count_sub")
    public List<IBeenCount> getAllSubmissions() {
        return marketing_submissionrepository.findByCountSubmission();
    }
    
    @GetMapping("/marketing_submission_count_int")
    public List<IBeenCount> getAllInterviews() {
        return marketing_submissionrepository.findByCountInterview();
    }
    
    @GetMapping("/marketing_submission_count_po")
    public List<IBeenCount> getAllPo() {
        return marketing_submissionrepository.findByCountPo();
    }
    
    @GetMapping("/marketing_submission_count_vc")
    public List<IBeenVC> getAllVendor() {
        return marketing_submissionrepository.findByVendorClient();
    }



	
}
