package com.project.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.demo.model.employee_role;
import com.project.demo.repository.employee_roleRepository;
@CrossOrigin(origins="*",allowedHeaders = "*")
@RestController
@RequestMapping("/data")
public class employee_roleController {
	

		@Autowired
	    private employee_roleRepository employee_rolerepository;

	    @GetMapping("/employee_role")
	    public List<employee_role> getAllEmp() {
	        return employee_rolerepository.findAll();

}
}
