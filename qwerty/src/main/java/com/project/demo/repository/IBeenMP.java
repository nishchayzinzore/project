package com.project.demo.repository;

import java.sql.Timestamp;

public interface IBeenMP {
String getTitle();
String getProjectType();
Timestamp getStart();

}
