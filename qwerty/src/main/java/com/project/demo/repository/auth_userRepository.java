package com.project.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.demo.model.auth_user;



public interface auth_userRepository extends JpaRepository<auth_user, Long>{

}
