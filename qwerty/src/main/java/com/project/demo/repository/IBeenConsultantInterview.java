package com.project.demo.repository;

public interface IBeenConsultantInterview 
{
	
	String getTitle();
	String getLastname();
	String getFirstname();
}