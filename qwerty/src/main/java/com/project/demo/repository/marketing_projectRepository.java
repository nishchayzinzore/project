package com.project.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.project.demo.model.marketing_project;

public interface marketing_projectRepository extends JpaRepository<marketing_project, Long> {

	@Query("select mp.title as title, mp.project_type as projectType, mp.start as start from marketing_project as mp inner join marketing_submission as ms on ms.id = mp.submission_id where ms.consultant_id=91")
	public List<IBeenMP> findByPO();
}
