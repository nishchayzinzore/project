package com.project.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.demo.model.employee_role;

public interface employee_roleRepository extends JpaRepository<employee_role, Long> {

}
