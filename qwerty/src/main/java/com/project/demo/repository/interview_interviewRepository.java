 package com.project.demo.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.project.demo.model.interview_interview;
public interface interview_interviewRepository extends JpaRepository<interview_interview, Long> {

	@Query("select a.first_name as first_name, a.last_name as last_name, i.title as title from auth_user as a inner join interview_interview as i on a.id=i.interview_supervisor_id inner join marketing_submission as m on i.submission_id=m.id where m.consultant_id=91")
	public List<IBeenII> findByInterviews();
}
