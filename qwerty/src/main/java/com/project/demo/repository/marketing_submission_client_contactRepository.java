package com.project.demo.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.project.demo.model.marketing_submission_client_contact;

public interface marketing_submission_client_contactRepository extends JpaRepository<marketing_submission_client_contact, Long> {

}
