package com.project.demo.repository;

public interface IBeenMarketer 
{   
	Long getMid();
	int getCount();
	String getFirstname();
	String getLastname();

}