package com.project.demo.repository;

public interface IBeenConsultantClient 
{
	int getSubmission_id();
	String getClient();
	
}