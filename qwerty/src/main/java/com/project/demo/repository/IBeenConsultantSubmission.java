package com.project.demo.repository;

public interface IBeenConsultantSubmission 
{  
	
	int getSubmission_id();
	String getJob_title();
	String getVendor();
	String getJlocation();
	String getMarketer_last_name();
	String getMarketer_first_name();

}