package com.project.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.project.demo.model.auth_user;

@Repository
public interface Marketer_List extends JpaRepository<auth_user, Long> {
	@Query
			("select a.id as mid, a.first_name as firstname, a.last_name as lastname,count(m.status) as count from auth_user as a \r\n" + 
			"inner join employee_employee as e on a.id = e.user_id \r\n" + 
			"inner join marketing_submission as m on e.id = m.employee_id\r\n" + 
			"where role_id in (select id from employee_role \r\n" + 
			"where title = 'Marketer' or title = 'Admin' or title='Ex Employee')\r\n" + 
			"and m.status in ('rate_conf', 'submitted', 'interview', 'po')\r\n" + 
			"group by a.id, a.first_name, a.last_name, a.id")
    public List<IBeenMarketer> findByMarketerSub();
	
	@Query
	("select a.id as mid,a.first_name as firstname, a.last_name as lastname,count(m.status) as count from auth_user as a \r\n" + 
			"inner join employee_employee as e on a.id = e.user_id \r\n" + 
			"inner join marketing_submission as m on e.id = m.employee_id\r\n" + 
			"where role_id in (select id from employee_role \r\n" + 
			"where title = 'Marketer' or title = 'Admin' or title='Ex Employee')\r\n" + 
			"and m.status in ('interview', 'po')\r\n" + 
			"group by a.id, a.first_name, a.last_name, a.id")
    public List<IBeenMarketer> findByMarketerInt();
	
	@Query
	("select a.id as mid,a.first_name as firstname, a.last_name as lastname,count(m.status) as count from auth_user as a \r\n" + 
			"inner join employee_employee as e on a.id = e.user_id \r\n" + 
			"inner join marketing_submission as m on e.id = m.employee_id\r\n" + 
			"where role_id in (select id from employee_role \r\n" + 
			"where title = 'Marketer' or title = 'Admin' or title='Ex Employee')\r\n" + 
			"and m.status = 'po'\r\n" + 
			"group by a.id, a.first_name, a.last_name, a.id")
    public List<IBeenMarketer> findByMarketerPo();
	
	
//	@Query("select t1.oid,t1.firstName,t1.lastName,t1.subcount,t2.intcount,t3.pocount\r\n" + 
//			"from (select a.id as oid, a.firstName, a.lastName,count(m.status) as subcount from User as a \r\n" + 
//			"inner join Employee as e on a.id = e.userId \r\n" + 
//			"inner join MarketingSub as m on e.id = m.employeeId\r\n" + 
//			"where role_id in (select id from EmployeeRole \r\n" + 
//			"where title = 'Marketer' or title = 'Admin' or title='Ex Employee')\r\n" + 
//			"and m.status in ('rate_conf', 'submitted', 'interview', 'po')\r\n" + 
//			"group by a.id, a.firsName, a.lastName) as t1,\r\n" + 
//			"(select a.id as pid, count(m.status) as intcount from User as a \r\n" + 
//			"inner join Employee as e on a.id = e.userId \r\n" + 
//			"inner join MarketingSub as m on e.id = m.employeeId\r\n" + 
//			"where role_id in (select id from EmployeeRole \r\n" + 
//			"where title = 'Marketer' or title = 'Admin' or title='Ex Employee')\r\n" + 
//			"and m.status in ('interview', 'po') \r\n" + 
//			"group by a.id, a.first_name, a.last_name\r\n" + 
//			") as t2,\r\n" + 
//			"(select a.id as qid, count(m.status) as pocount from User as a \r\n" + 
//			"inner join Employee as e on a.id = e.userId \r\n" + 
//			"inner join MarketingSub as m on e.id = m.employeeId\r\n" + 
//			"where role_id in (select id from EmployeeRole \r\n" + 
//			"where title = 'Marketer' or title = 'Admin' or title='Ex Employee')\r\n" + 
//			"and m.status = 'po' group by a.id, a.firstName, a.lastName, a.id\r\n" + 
//			") as t3\r\n" + 
//			"where t1.oid=t2.pid and t1.oid=t3.qid")
//    public List<?> findByMarketerAll();
}



