package com.project.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.demo.model.marketing_clientcontact;

public interface marketing_clientcontactRepository extends JpaRepository<marketing_clientcontact, Long> {

}
