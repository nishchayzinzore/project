package com.project.demo.repository;

public interface IBeenConsultantInteriew {
	
	String getTitle();
	String getLastname();
	String getFirstname();
}
