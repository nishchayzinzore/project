package com.project.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.project.demo.model.marketing_submission;

public interface marketing_submissionRepository extends JpaRepository<marketing_submission, Long> {

	@Query("select extract(month from created) as MONTH, COUNT(*) as COUNT from marketing_submission where status in('rate_conf', 'submitted', 'interview', 'po') and extract(year from created)=2018 group by extract(year from created), extract(month from created)")
    public List<IBeenCount> findByCountSubmission();
  
	@Query("select extract(month from created) as MONTH, count(*) as COUNT from marketing_submission where status in('interview', 'po') and extract(year from created)=2018 group by extract(year from created), extract(month from created)")
	public List<IBeenCount> findByCountInterview();
	
	@Query("select extract(month from created) as MONTH, count(*) as COUNT from marketing_submission where status = 'po' and extract(year from created)=2018 group by extract(year from created), extract(month from created)")
	public List<IBeenCount> findByCountPo();
	
	@Query("select m.id as submission_Id, m.title as job_Title, cl.title as vendor, m.job_location as job_Location, a.first_name as marketer_First_Name, a.last_name as marketer_Last_Name from marketing_submission as m inner join marketing_submission_client_contact as mscc on m.id=mscc.submission_id inner join marketing_clientcontact as cc on mscc.clientcontact_id=cc.id inner join marketing_client as cl on cc.client_id=cl.id inner join auth_user as a on m.employee_id=a.id inner join consultants_consultant as con on con.user_id=a.id where cl.level=1 and con.id=91 ")
	public List<IBeenVC> findByVendorClient();
	

	
}
