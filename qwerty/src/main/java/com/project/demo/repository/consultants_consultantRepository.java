package com.project.demo.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.project.demo.model.consultants_consultant;
public interface consultants_consultantRepository extends JpaRepository<consultants_consultant, Long>{

	

@Query("select c.id as id, a.first_name as first_name, a.last_name as last_name, c.ssn as ssn, a1.first_name as r_fname, a1.last_name as r_lname from auth_user as a inner join consultants_consultant as c on a.id=c.user_id inner join auth_user as a1 on a1.id=c.recruiter_id group by a.first_name, a.last_name, c.ssn, a1.first_name, a1.last_name, c.id")
public List<IBeenCC> findByConsultants();
	

	

@Query("select m.id as submission_id, m.title as job_title, cl.title as vendor, m.job_location as jlocation, a.first_name as marketer_first_name, a.last_name as marketer_last_name from marketing_submission as m inner join marketing_submission_client_contact as mscc on m.id = mscc.submission_id inner join marketing_clientcontact as cc on mscc.clientcontact_id = cc.id inner join marketing_client as cl on cc.client_id = cl.id inner join auth_user as a on m.employee_id = a.id inner join consultants_consultant as con on con.user_id = a.id where cl.level = 1 and con.id =(:cid)")
public List<IBeenConsultantSubmission>findByConsultantsub(int cid);

@Query("select m.id as submission_id, cl.title as client\r\n" + 
		"from   marketing_submission as m \r\n" + 
		"inner join marketing_submission_client_contact as mscc on m.id = mscc.submission_id\r\n" + 
		"inner join marketing_clientcontact as cc on mscc.clientcontact_id = cc.id\r\n" + 
		"inner join marketing_client as cl on cc.client_id = cl.id\r\n" + 
		"inner join auth_user as a on m.employee_id = a.id\r\n" + 
		"inner join consultants_consultant as con on con.user_id = a.id\r\n" + 
		"where cl.level = 2 and con.id = (:cid)")
public List<IBeenConsultantClient>findByConsultantcli(int cid);


@Query("select a.first_name as firstname, a.last_name as lastname, i.title as title from auth_user as a \r\n" + 
		"inner join interview_interview as i on a.id = i.interview_supervisor_id \r\n" + 
		"inner join marketing_submission as m on i.submission_id = m.id \r\n" + 
		"where m.consultant_id =(:cid)")
public List<IBeenConsultantInterview>findByConsultantint(int cid);


@Query("select mp.title as title, mp.project_type as project, mp.start as starte from marketing_project as mp\r\n" + 
		"inner join marketing_submission as ms on ms.id = mp.submission_id\r\n" + 
		"where ms.consultant_id = (:cid)")
public List<IBeenConsultantPo>findByConsultantspo(int cid);

	}