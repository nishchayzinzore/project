package com.project.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.demo.model.marketing_client;

public interface marketing_clientRepository extends JpaRepository<marketing_client, Long >{

}
