import React from 'react';
import 'antd/dist/antd.css';
import ReactTable from "react-table";
import { Layout, Menu, Breadcrumb, Icon } from "antd";
import "react-table/react-table.css"
const { Content, Sider } = Layout;



class TableInterview extends React.Component{
    constructor(props){
        super(props);
        this.state={
        posts: [],
        showComponent: false,
        }
    } 
   
    componentDidMount(){
      const url="http://127.0.0.1:8000/data/consultants_consultant_int/"+this.props.id;
      fetch(url, {method: "GET" , headers:{
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true
         } })
         .then(response => response.json()).catch(function() {
          console.log("error");
      })
         .then(posts => {
           console.log("posts",posts)
           this.setState({posts:posts}) 
         })
        
    } 

    render(){
      const columns = [
        {
          Header: "Job Title",
          accessor: "title"
        },
        {
          Header: "Marketer Name",
          accessor: "firstname"
        },
        {
          Header: "Marketer Last Name",
          accessor: "lastname"
        }
      ];
        return(
          <ReactTable columns={columns} data={this.state.posts} />
        );
    }
}

export default TableInterview