import React from "react";
import ReactDOM from "react-dom";
import "antd/dist/antd.css";
//import "./index.css";
import { Table,Layout, Input, Button, Icon } from "antd";
import ReactTable from "react-table";
import "react-table/react-table.css";
import Highlighter from "react-highlight-words";

const {Content, Sider}=Layout;



class consultant_table extends React.Component {
  constructor(props) {
   super();
   this.state = {
   posts: []
   }
  }

  componentDidMount(){

    fetch('http://127.0.0.1:8000/data/consultants_consultant',{
      method:'GET',headers:{'Content-Type':'application.json',
    "Access-Control-Allow-Origin":"*",
    "Access-Control-Allow-Credentials":true
  
  }
    })
    .then(response=> response.json()).then(posts=>{
      console.log("posts",posts)
        this.setState({"posts":posts})
    }).catch(function(){
      console.log("error");
    })
  }
   


  render() {
    
    const columns = [
        {
          Header: "Consultant_id",
          accessor: "id",
          key: "id",
        },
        {
          Header: "FirstName",
          accessor: "first_name",
          key: "first_name",
        },
        {
          Header: "LastName",
          accessor: "last_name",
          key: "last_name",
        },
        {
          Header: "SSN",
          accessor: "ssn",
          key: "ssn",
        },
        {
          Header: "Recruiter_fname",
          accessor: "r_fname",
          key: "r_fname",
        },
        {
          Header: "Recruiter_lname",
          accessor: "r_lname",
          key: "r_lname",
        }
        
    ];




return(
  <Layout>


<Content style={{ margin: "0 16px",padding: 24, background: "#fff", minHeight: 360}}>
              <div style={{ padding: 24, background: "#fff", minHeight: 360 }}>
              <ReactTable columns={columns} data={this.state.posts} />;
              </div>
            </Content>

  


    </Layout>
);

  }
}



export default consultant_table;

