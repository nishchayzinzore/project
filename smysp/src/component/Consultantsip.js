import React from 'react';
import 'antd/dist/antd.css';
import { Layout, Menu, Breadcrumb, Icon } from "antd";
import { Tabs } from 'antd';
import { HashRouter, BrowserRouter as Router} from 'react-router-dom';
import TableSubmission from './TableSubmission';
import TableInterview from './TableInterview';
import TablePo from './TablePo';

const { TabPane } = Tabs;
const { Content } = Layout;
const { SubMenu } = Menu;
class CallBack extends React.Component{
    constructor(props)
    {
        super(props);
        console.log("id",this.props.id)
    }
 callback(key) {
  console.log(key);
}

render(){
    return(<HashRouter>
        <Layout>
        <Content style={{ margin: "0 16px" }}>
         <Breadcrumb style={{ margin: "16px 0" }}>
         </Breadcrumb>
         <div style={{ padding: 24, background: "#fff", minHeight: 360 }}>
         <Tabs defaultActiveKey="1" >
            <TabPane tab="Submissions" key="1">
            <TableSubmission id={this.props.id}/>
            </TabPane>
            <TabPane tab="Interview" key="2">
            <TableInterview id={this.props.id} />
            </TabPane>
            <TabPane tab="P O" key="3">
            <TablePo id={this.props.id} />
            </TabPane>
        </Tabs>
         </div>
       </Content>
     </Layout>
     </HashRouter>
        
    )

    }
}        
export default CallBack