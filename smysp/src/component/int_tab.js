import React from 'react';
import 'antd/dist/antd.css';
import { Table, Divider } from 'antd';
import ReactTable from "react-table";
import "react-table/react-table.css";
import { Layout, Menu, Breadcrumb, Icon } from "antd";

const { Content, Sider } = Layout;


class Int_Tab extends React.Component{
    constructor(props){
        super(props);
        this.state={
        posts:[],
        
        }
    } 

    componentDidMount()
    {
      const url="http://127.0.0.1:8000/data/marketerint";
      fetch(url, {method: "GET" , headers:{
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true
         } 
        })
         .then(response => response.json()).catch(function() {
          console.log("error");
      })
         .then(posts => {
           console.log("posts",posts)
           this.setState({posts:posts}) 
         })



    } 



    // async componentDidMount()
    // {
      
    //   fetch('http://127.0.0.1:8000/api/v1/consultant/',
    //          {method:'GET',headers:{
    //           'Content-Type': 'application.json',
    //           "Access-Control-Allow-Origin":"*",
    //           "Access-Control-Allow-Credentials": true
    //          }}
    //   )
  
    //     .then(response => response.json())
    //     .then((response)=>{
    //       console.log(response)
    //       this.setState({posts:response})
    //     })
      
    //      .catch(error =>{
    //        console.log(error)
    //      })
  
    //     )
           
    // }

    render(){
      const columns = [
        {
          Header: " Id",
          accessor: "mid"
        },
        {
          Header: "First Name",
          accessor: "firstname"
        },
        {
          Header: "Last Name",
          accessor: "lastname"
        },
        {
          Header: "Submission",
          accessor: "count"
        },
        
      ];


        return(
          <Layout>
         <Content style={{ margin: "0 16px" }}>
          <Breadcrumb style={{ margin: "16px 0" }}>
            <Breadcrumb.Item>
              <Icon type="close" /> CONSULTANTS
            </Breadcrumb.Item>
          </Breadcrumb>
          <div style={{ padding: 24, background: "#fff", minHeight: 360 }}>
          <ReactTable columns={columns} data={this.state.posts} />
        
          </div>
        </Content>
      </Layout>
        );
    }
}

export default Int_Tab