import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import { Table, Divider, Button } from 'antd';
import ReactTable from "react-table";
import { Layout, Menu, Breadcrumb, Icon } from "antd";
import "react-table/react-table.css"
const { Content, Sider } = Layout;



class TableSubmission extends React.Component{
    constructor(props){
        super(props);
        this.state={
        posts: [],
        showComponent: false,
        }
    } 
   
    componentDidMount(){
      const url="http://127.0.0.1:8000/data/consultants_consultant_sub/"+this.props.id;
      fetch(url, {method: "GET" , headers:{
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true
         } })
         .then(response => response.json()).catch(function() {
          console.log("error");
      })
         .then(posts => {
           console.log("posts",posts)
           this.setState({posts:posts}) 
         })
        
    } 

    render(){
      const columns = [
        {
          Header: "Submission Id",
          accessor: "submission_id"
        },
        {
          Header: "Job Title",
          accessor: "job_title"
        },
        {
          Header: "Vndor",
          accessor: "vendor"
        },
        {
          Header: "Marketer Name",
          accessor: "marketer_first_name"
        },
        {
          Header: "Marketer Last Name",
          accessor: "marketer_last_name"
        }
      ];
        return(
          <ReactTable columns={columns} data={this.state.posts} />
        );
    }
}

export default TableSubmission