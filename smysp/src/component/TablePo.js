import React from 'react';
import 'antd/dist/antd.css';
import ReactTable from "react-table";
import "react-table/react-table.css";

class TablePo extends React.Component{
    constructor(props){
        super(props);
        this.state={
        posts: [],
        showComponent: false,
        }
    } 
   
    componentDidMount(){
      const url="http://127.0.0.1:8000/data/consultants_consultant_po/"+this.props.id;
      console.log("url",url)
      fetch(url, {method: "GET" , headers:{
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true
         } })
         .then(response => response.json()).catch(function() {
          console.log("error");
      })
         .then(posts => {
           console.log("posts",posts)
           this.setState({posts:posts}) 
         })
        
    } 

    render(){
      const columns = [
        {
            Header: "Title",
            accessor: "titlee"
          },
          {
            Header: "Project Type",
            accessor: "project"
          },
          {
            Header: "Started At",
            accessor: "starte"
          }
      ];
        return(
          <ReactTable columns={columns} data={this.state.posts} />
        );
    }
}

export default TablePo