import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import {
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend
} from "recharts";
import { scaleOrdinal } from "d3-scale";
import { schemeCategory10 } from "d3-scale-chromatic";

const colors = scaleOrdinal(schemeCategory10).range();

// const data = [
//   {
//     name: "JAN",
//     uv: 4000,
//     female: 2400,
//     male: 2400
//   },
//   {
//     name: "FEB",
//     uv: 3000,
//     female: 1398,
//     male: 2210
//   },
//   {
//     name: "MAR",
//     uv: 2000,
//     female: 9800,
//     male: 2290
//   },
//   {
//     name: "APR",
//     uv: 2780,
//     female: 3908,
//     male: 2000
//   },
//   {
//     name: "MAY",
//     uv: 1890,
//     female: 4800,
//     male: 2181
//   },
//   {
//     name: "JUN",
//     uv: 2390,
//     female: 3800,
//     male: 2500
//   },
//   {
//     name: "JUL",
//     uv: 3490,
//     female: 4300,
//     male: 2100
//   },
//   {
//     name: "AUG",
//     uv: 3490,
//     female: 4300,
//     male: 2100
//   },{
//     name: "SEP",
//     uv: 3490,
//     female: 4300,
//     male: 2100
//   },{
//     name: "OCT",
//     uv: 3490,
//     female: 4300,
//     male: 2100
//   },
//   {
//     name: "NOV",
//     uv: 3490,
//     female: 4300,
//     male: 2100
//   },
//   {
//     name: "Dec",
//     uv: 3490,
//     female: 4300,
//     male: 2100
//   }
// ];

const getPath = (x, y, width, height) => `M${x},${y + height}
          C${x + width / 3},${y + height} ${x + width / 2},${y +
  height / 3} ${x + width / 2}, ${y}
          C${x + width / 2},${y + height / 3} ${x + (2 * width) / 3},${y +
  height} ${x + width}, ${y + height}
          Z`;

const TriangleBar = props => {
  const { fill, x, y, width, height } = props;

  return <path d={getPath(x, y, width, height)} stroke="none" fill={fill} />;
};

TriangleBar.propTypes = {
  fill: PropTypes.string,
  x: PropTypes.number,
  y: PropTypes.number,
  width: PropTypes.number,
  height: PropTypes.number
};

export default class Submission_Chart extends PureComponent {
  static jsfiddleUrl = "https://jsfiddle.net/alidingling/rnywhbu8/";
  constructor(props){
  
    super(props)
     this.state={
       posts: []
     }
    
    }
  async componentDidMount()
 
  

  {
    fetch('http://127.0.0.1:8000/data/marketing_submission_count_sub',
     {method:'GET',headers:{
       'Content-Type':'application.json',
       "Access-Control-Allow-Origin":"*",
       "Acess-Control-Allow-Credentials":true
     }})


     .then(response => response.json())
     .then((response)=>{

  console.log(response)
   this.setState({posts:response})
     })

   
    .catch(error =>{console.log(error)
   })

 }


  render() {
    const { posts }=this.state
    return (
      <BarChart
        width={1100}
        height={500}
        data={posts}
        margin={{
          top: 20,
          right: 30,
          left: 20,
          bottom: 5
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="month" />
        <YAxis />
        <Bar
          dataKey="count"
          fill="#8884d8"
          shape={<TriangleBar />}
          label={{ position: "top" }}
        >
          {posts.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={colors[index % 20]} />
          ))}
        </Bar>
      </BarChart>
    );
  }
}
