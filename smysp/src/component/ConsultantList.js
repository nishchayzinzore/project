import React from 'react';
import 'antd/dist/antd.css';
import ReactTable from "react-table";
import { Layout, Menu, Breadcrumb, Icon } from "antd";
import "react-table/react-table.css"
import CallBack from './Consultantsip'
import { HashRouter, BrowserRouter as Router} from 'react-router-dom';


const { Content, Sider } = Layout;



class Consultant extends React.Component{
    constructor(props){
        super(props);
        this.state={
        posts: [],
        showComponent: false,
        id:-1
        }
        this.onClickButton=this.onClickButton.bind(this);
        this.showTabs=this.showTabs.bind(this);
        this.onClickBack=this.onClickBack.bind(this);
    } 
    onClickButton(id)
    {
      this.setState({
        showComponent:true,
        id:id
       
      })
    }
    onClickBack()
    {
      document.getElementById('1').style.display= 'block';
      document.getElementById('2').style.display='none';
    }

    showTabs()
    {
      document.getElementById('2').style.display= 'block';
      document.getElementById('1').style.display='none';
      console.log("showtabs running ")
    }

    componentDidMount(){
      const url="http://127.0.0.1:8000/data/consultants_consultant/";
      fetch(url, {method: "GET" , headers:{
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true
         } })
         .then(response => response.json()).catch(function() {
          console.log("error");
      })
         .then(posts => {
           console.log("posts",posts)
           this.setState({posts:posts}) 
         })
        
    } 

    render(){
      const columns = [
        {
          Header: "Consultant Id",
          accessor: "id"
        },
        {
          Header: "First Name",
          accessor: "first_name"
        },
        {
          Header: "Last Name",
          accessor: "last_name"
        },
        {
          Header: "SSN",
          accessor: "ssn"
        },
        {
          Header: "RecruiterFirstName",
          accessor: "r_fname"
        },
        {
            Header: "RecruiterLastName",
            accessor: "r_lname"
          },
        {
          Header:"Select",
          accessor: "id",
          Cell : row =>(
            <div>
              {console.log(row)}
              <button onClick={()=>this.onClickButton(row.value)}>click</button>
              {this.state.showComponent ?  this.showTabs(): null}
            </div>
          )
        } 
      ];


        return(
          <HashRouter>
          <Layout>
         <Content style={{ margin: "0 16px" }}>
          <Breadcrumb style={{ margin: "16px 0" }}>
            <Breadcrumb.Item>
              <button onClick={()=>this.onClickBack()}><Icon type="left" /></button> CONSULTANTS
            </Breadcrumb.Item>
          </Breadcrumb>
          <div id="1" style={{ padding: 24, background: "#fff", minHeight: 360 }}>
          <ReactTable columns={columns} data={this.state.posts} />
          </div>
          <div id="2">
            {console.log("id-----------------------",this.state.id)}
            {this.state.showComponent &&
            <CallBack id={this.state.id}/>
            }
          </div>
        </Content>
      </Layout>
      </HashRouter>
        );
    }
}

export default Consultant