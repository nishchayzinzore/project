import React from "react";
import ReactDOM from "react-dom";
import "antd/dist/antd.css";
import "./index.css";
import { Layout, Menu, Breadcrumb, Icon } from "antd";
import Submission_Chart from "./component/submission_chart";
import Interview_Chart from "./component/interview_chart";
import PO_Chart from "./component/po_chart";
import consultant_table from "./component/consultant_table";
import marketing_table from "./component/marketing_table";
import PO_Tab from "./component/po_tab";
import Int_Tab from "./component/int_tab";
import Sub_Tab from "./component/sub_tab";
import axios from 'axios';
import Consultant from "./component/ConsultantList"


import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

class SiderDemo extends React.Component {
  state = {
    collapsed: false
  };

  onCollapse = collapsed => {
    console.log(collapsed);
    this.setState({ collapsed });
  };

  render() {
    return (
      <Router>
        <Layout style={{ minHeight: "100vh" }}>
          <Sider
            collapsible
            collapsed={this.state.collapsed}
            onCollapse={this.onCollapse}
          >
            <div className="logo" />
            <Menu >
              <SubMenu
                key="sub1"
                title={
                  <span>
                    <Icon type="desktop" />
                    <span>Dashboard</span>
                  </span>
                }
              >
                <Menu.Item>
                  {/* {" "} */}
                  <Link to={"/submission"} className="Submission_Chart">
                    Submission
                  </Link>
                </Menu.Item>
                <Menu.Item>
                  <Link to={"/interview"} className="Interview_Chart">
                    Interview
                  </Link>
                </Menu.Item>
                <Menu.Item>
                  <Link to={"/po"} className="PO_Chart">
                    PO
                  </Link>
                </Menu.Item>
              </SubMenu>
              <SubMenu
                
                title={
                  <span>
                    <Icon type="user" />
                    <span>Marketer</span>
                  </span>
                }
              >
                <Menu.Item>
                  {/* {" "} */}
                  <Link to={"/subtab"} className="Sub_Tab">
                    Submission
                  </Link>
                </Menu.Item>
                <Menu.Item>
                  <Link to={"/inttab"} className="Int_Tab">
                    Interview
                  </Link>
                </Menu.Item>
                <Menu.Item>
                  <Link to={"/potab"} className="PO_Tab">
                    Purchase Order
                  </Link>
                </Menu.Item>
              </SubMenu>

              {/* <Menu.Item>
                
                <Icon type="user" />
                <span>
                  <Link to={"/ct"} className="consultant_table">
                    Consultant
                  </Link>
                </span>
              </Menu.Item> */}
              <Menu.Item >
                     {" "}
                     <Link to={"/con"} className="Consultant">
                     <Icon type="user" />
                      Consultant
                     </Link>
                    </Menu.Item> 
              {/* <Menu.Item>
                <Icon type="desktop" />
                <span>
                <Icon type="user" />
                  <Link to={"/mt"} className="marketing_table">
                    Marketer
                  </Link>
                </span>
              </Menu.Item> */}
            </Menu>
          </Sider>
          <Layout>
            <Header style={{ background: "#fffff", padding: 0 }} />
            <Content style={{ margin: "0 16px" }}>
              <div style={{ padding: 24, background: "#fffff", minHeight: 360 }}>
                <Switch>
                  <Route
                    exact
                    path="/submission"
                    component={Submission_Chart}
                  />
                  <Route exact path="/interview" component={Interview_Chart} />
                  <Route exact path="/po" component={PO_Chart} />
                  <Route exact path="/ct" component={consultant_table} />
                  <Route exact path="/mt" component={marketing_table} />
                  <Route exact path="/potab" component={PO_Tab} />
                  <Route exact path="/inttab" component={Int_Tab} />
                  <Route exact path="/subtab" component={Sub_Tab} />
                  <Route exaxt path="/con" component={Consultant} />


                </Switch>
              </div>
            </Content>
            <Footer style={{ textAlign: "center" }}>
              Ant Design ©2018 Created by Ant UED
            </Footer>
          </Layout>
        </Layout>
      </Router>
    );
  }
}

ReactDOM.render(<SiderDemo />, document.getElementById("container"));
